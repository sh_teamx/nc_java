package ua.edu.sumdu.java.shestak.pr6;

import ua.edu.sumdu.java.shestak.pr5.AbstractTaskList;
import ua.edu.sumdu.java.shestak.pr5.ArrayTaskList;
import ua.edu.sumdu.java.shestak.pr5.Task;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by shestak.maksym on 20.01.17.
 */
public class Tester {
    public static final long SECOND = 1000;
    public static final long MINUTE = SECOND * 60;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;

    public static void main(String[] args) {


        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            Date date1 = sdf.parse("29-10-2016 12:05");
            Date date2 = sdf.parse("30-10-2016 13:34");

            Task task1 = new Task("t1", date1);
            Task tastRepeated = new Task("repeated", sdf.parse("29-10-2016 10:35"), sdf.parse("03-11-2016 11:40"), 10000*MINUTE)
                    .setActive(true);

            AbstractTaskList abstractTaskList = new ArrayTaskList();
            abstractTaskList.add(task1);
            abstractTaskList.add(tastRepeated);

            //File file = new File("binary.txt");
            //TaskIO.writeText(abstractTaskList, file);

            System.out.println(tastRepeated.toString());
            System.out.println(TaskIO.parseTask(tastRepeated.toString()).toString());

//            System.out.println(10000*MINUTE);
//            System.out.println(TaskIO.parseRepeatInterval("6 days 22 hours 40 minutes"));

        } catch (ParseException e) {
            e.printStackTrace();
        }


    }
}
