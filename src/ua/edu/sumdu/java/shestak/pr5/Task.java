package ua.edu.sumdu.java.shestak.pr5;

/**
 * @author <a href="mailto:slvr.max@gmail.com">Maksym Shestak</a>
 * @since 02.06.2016
 */

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

/**
 * The class that describes the type "task" data,
 * which contains information about the essence of the problem,
 * its status (active / inactive), the time interval warning
 * time, through which you need to repeat the warning about it
 */
@SuppressWarnings("Duplicates")
public class Task implements Cloneable, Serializable {

    private String title;
    private boolean active;

    private Date time, startTime, endTime;
    long repeatTime;

    public Task() {
        super();
    }

    /**
     * The constructor for a one-time task
     *
     * @param title task title
     * @param time  time on task notifications
     */
    public Task(String title, Date time) throws IllegalArgumentException {
        setTitle(title);
        setTime(time);
        setActive(false);
    }

    /**
     * The constructor for a recurring task
     *
     * @param title      task title
     * @param startTime  the start time of the notification
     * @param endTime    time alerting end
     * @param repeatTime the time after which you must repeat the notification
     */
    public Task(String title, Date startTime, Date endTime, long repeatTime) throws IllegalArgumentException {
        setTitle(title);
        setTime(startTime, endTime, repeatTime);
        setActive(false);
    }

    /**
     * Method for getting task title
     *
     * @return task title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Method for setting task title
     *
     * @param title task title
     */
    public Task setTitle(String title) throws IllegalArgumentException {
        if(title.equals("") || title == null) throw new IllegalArgumentException();
        this.title = title;
        return this;
    }

    /**
     * Method for getting task status
     *
     * @return status
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Method for setting task  status
     *
     * @param active статус задачи
     */
    public Task setActive(boolean active) {
        this.active = active;
        return this;
    }

    /**
     * The method for getting start time alerts (for repetitive tasks) or single alarm time (for a one-time task)
     *
     * @return task time notification
     */
    public Date getTime() {
        if(repeatTime != 0) return startTime;
        return time;
    }

    /**
     * The method of setting the start time for the one-time task, where time - time notifications about the task
     *
     * @param time time notifications about the task
     */
    public Task setTime(Date time) throws IllegalArgumentException {
        this.time = time;
        this.startTime = time;
        this.endTime = time;
        this.repeatTime = 0;
        return this;
    }

    /**
     * The method of setting the start time alerts for repeating tasks,
     *
     * @param start  notification start time
     * @param end    notification end time
     * @param repeat the time interval at which you need to repeat notification
     */
    public Task setTime(Date start, Date end, long repeat) throws IllegalArgumentException {
        this.time = start;
        this.startTime = start;
        this.endTime = end;
        this.repeatTime = repeat;
        return this;
    }

    /**
     * Method for getting the start time of notification (for repetitive tasks)
     * or a time of single notification (for a one-time task)
     *
     * @return task start time
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Method for getting the end-time of notification (for repetitive tasks)
     * or a time of single notification (for a one-time task)
     *
     * @return task end time
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Method for getting the time interval at which you need to repeat the notification
     * about the task (for repetitive tasks) or 0 (for a one-time task)
     *
     * @return repeat interval
     */
    public long getRepeatInterval() {
        return repeatTime;
    }

    /**
     * A method for obtaining information about whether the task is repeated
     *
     * @return repeat status
     */
    public boolean isRepeated() {
        return repeatTime > 0;
    }

    /**
     * The method returns the next notification, after a specified time period (but not including)
     *
     * @param time time
     * @return time of next notification
     */


    public Date nextTimeAfter(Date time) throws IllegalArgumentException {
        if(time == null)
            throw new IllegalArgumentException();

        Date nextTime = null;

        if(isActive()) {

            // for no-repeat task
            if(!isRepeated() && time.before(this.time) /*time < this.time*/)
                nextTime = this.time;

            //for repeated task
            else {

                if(time.before(startTime) /*time < startTime*/)
                    nextTime = startTime;

                else {
                    nextTime = startTime;
                    while(nextTime.compareTo(time) <= 0 /*nextTime <= time*/) {

                        //nextTime += repeatTime;
                        nextTime.setTime(nextTime.getTime() + repeatTime);
                    }
                    if(nextTime.after(endTime) /*nextTime > endTime*/) nextTime = null;
                }
            }
        }

        return nextTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS.sss");

        sb.append("\"");
        sb.append(title);
        sb.append("\"");
        if(isRepeated()) {
            sb.append(" from [");
            sb.append(sdf.format(startTime));
            sb.append("] to [");
            sb.append(sdf.format(endTime));
            sb.append("] every [");
            sb.append(getRepeat(repeatTime));
            sb.append("]");

        } else {
            sb.append(" at [");
            sb.append(sdf.format(time));
            sb.append("]");
        }
        if(!isActive()) sb.append(" inactive");

        return sb.toString();
    }

    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 24 * HOUR;

    private String getRepeat(long ms) {
        StringBuffer text = new StringBuffer("");
        if (ms >= DAY) {
            text.append(ms / DAY);
            if(ms / DAY > 1)    text.append(" days");
            else                text.append(" day");
            ms %= DAY;
            if(ms != 0) text.append(' ');
        }
        if (ms > HOUR) {
            text.append(ms / HOUR);
            if(ms / HOUR > 1)    text.append(" hours");
            else                text.append(" hour");
            ms %= HOUR;
            if(ms != 0) text.append(' ');
        }
        if (ms > MINUTE) {
            text.append(ms / MINUTE);
            if(ms / MINUTE > 1)    text.append(" minutes");
            else                text.append(" minute");
            ms %= MINUTE;
            if(ms != 0) text.append(' ');
        }
        if (ms > SECOND) {
            text.append(ms / SECOND);
            if(ms / SECOND > 1)    text.append(" seconds");
            else                text.append(" second");
            ms %= SECOND;
        }
        //text.append(ms + " ms");
        return text.toString();
    }
}
