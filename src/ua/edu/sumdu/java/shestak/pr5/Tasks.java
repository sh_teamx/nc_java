package ua.edu.sumdu.java.shestak.pr5;

import java.util.*;

/**
 * Created by shestak.maksym on 29.10.16.
 */
public class Tasks {


    public static Iterable<Task> incoming(Iterable<Task> tasks, Date start, Date end) {

        AbstractTaskList incomingTasks;
        if(tasks instanceof ArrayTaskList) incomingTasks = new ArrayTaskList();
        else incomingTasks = new LinkedTaskList();

        for(Task t : tasks)
            if(t.nextTimeAfter(start).after(start) && t.nextTimeAfter(end).before(end))
                incomingTasks.add(t);


        return incomingTasks;
    }


    SortedMap<Date, Set<Task>> calendar(Iterable<Task> tasks, Date from, Date to) {

        if (from == null) throw new IllegalArgumentException("from = null");
        if (to == null) throw new IllegalArgumentException("to = null");
        if (to.before(from))
            throw new IllegalArgumentException("end < start");

        SortedMap<Date, Set<Task>> calendar = new TreeMap<Date, Set<Task>>();

        Iterable<Task> incoming = incoming(tasks, from, to);

        for (Task task : incoming) {

            Date timeTask = task.nextTimeAfter(from);
            while (timeTask != null && !timeTask.after(to)) {
                if (calendar.containsKey(timeTask)) {
                    calendar.get(timeTask).add(task);
                } else {
                    Set<Task> setTasks = new HashSet<Task>();
                    setTasks.add(task);
                    calendar.put(timeTask, setTasks);
                }
                timeTask = task.nextTimeAfter(timeTask);
            }

        }

        return calendar;
    }

}
