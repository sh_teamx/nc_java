package ua.edu.sumdu.java.shestak.pr5;

import ua.edu.sumdu.java.shestak.pr6.TaskIO;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by shestak.maksym on 27.10.16.
 */
public class Tester {

    public static final long SECOND = 1000;
    public static final long MINUTE = SECOND * 60;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;


    public static void main(String[] args) {


        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");


            Date date1 = sdf.parse("29-10-2016 12:05");

            Date date2 = sdf.parse("30-10-2016 13:34");

            Task task1 = new Task("t1", date1);
            Task tastRepeated = new Task("repeated", sdf.parse("29-10-2016 10:35"), sdf.parse("03-11-2016 11:40"), DAY)
                    .setActive(true);


            AbstractTaskList abstractTaskList = new ArrayTaskList();
            abstractTaskList.add(task1);
            abstractTaskList.add(tastRepeated);

            TaskIO.writeText(abstractTaskList, new File("text.txt"));


        } catch (ParseException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
